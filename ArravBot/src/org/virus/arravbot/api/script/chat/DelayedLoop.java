package org.virus.arravbot.api.script.chat;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/1/13
 * Time: 3:48 PM
 */
public interface DelayedLoop extends Runnable {
    /**
     * @return delay time in ms.
     */
    public int getDelay();
}
