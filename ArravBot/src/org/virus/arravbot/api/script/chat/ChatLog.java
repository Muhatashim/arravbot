package org.virus.arravbot.api.script.chat;

import org.virus.arravbot.api.client.Client;
import org.virus.arravbot.api.client.reflection.Hook;

import java.lang.ref.SoftReference;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/1/13
 * Time: 3:26 PM
 */
public class ChatLog implements DelayedLoop {
    private static List<ChatListener>     listeners = new ArrayList<>();
    private static SoftReference<ChatLog> instance  = new SoftReference<>(null);

    private String[] lastTexts;
    private String[] texts;
    private String[] lastNames;
    private String[] names;

    private ChatLog(Client client) throws InvocationTargetException, IllegalAccessException {
        Object clientInstance = client.getHooker().get(Hook.HookType.CLIENT).getValue(null);

        texts = (String[]) client.getHooker().get(Hook.HookType.CHAT_LOG).getValue(clientInstance);
        names = (String[]) client.getHooker().get(Hook.HookType.CHAT_NAMES).getValue(clientInstance);
        lastTexts = new String[texts.length];
        lastNames = new String[names.length];
    }

    public static ChatLog get() {
        try {
            return instance.get() != null ?
                    instance.get() :
                    (instance = new SoftReference<>(new ChatLog(Client.get()))).get();
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void addListener(ChatListener listener) {
        listeners.add(listener);
    }

    private static int findIndexRefSame(String[] arr0, String[] arr1) {
        for (int i = 0; i < arr1.length; i++)
            if (arr0[0] == arr1[i])
                return i;

        return arr0.length == arr1.length ? -1 : arr0.length;
    }

    private static void copyReferences(Object[] from, Object[] to) {
        for (int i = 0; i < from.length; i++)
            to[i] = from[i];
    }

    private void fire(MessageEvent me) {
        for (ChatListener listener : listeners) {
            listener.onMessageRecieved(me);
        }
    }

    @Override
    public void run() {
        int indexRefMemSameTexts = findIndexRefSame(lastTexts, texts);
        int indexRefRefSameNames = findIndexRefSame(names, lastNames);
        int indexRefMemSame = Math.max(indexRefRefSameNames, indexRefMemSameTexts);

        if (indexRefMemSame != -1) {
            for (int i = indexRefMemSame - 1; i >= 0; i--) {
                if (names[i] == null && texts[i] == null)
                    continue;

                fire(new MessageEvent(names[i], texts[i]));
            }

            copyReferences(texts, lastTexts);
            copyReferences(names, lastNames);
        }
    }

    @Override
    public int getDelay() {
        return 100;
    }
}
