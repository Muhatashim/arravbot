package org.virus.arravbot.api.script.chat;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/1/13
 * Time: 3:36 PM
 */
public class MessageEvent {

    private final String name;
    private final String message;

    public MessageEvent(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "MessageEvent{" +
                "name='" + name + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
