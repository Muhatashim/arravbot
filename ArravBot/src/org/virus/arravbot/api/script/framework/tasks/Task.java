package org.virus.arravbot.api.script.framework.tasks;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 11:18 AM
 */
public interface Task {
    /**
     * @return True if the task is still valid, false otherwise.
     */
    public boolean valid();

    /**
     * @return Max waiting time to wait before stopping this task in ms.
     */
    public int maxLagTime();
}
