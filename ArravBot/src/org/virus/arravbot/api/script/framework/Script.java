package org.virus.arravbot.api.script.framework;

import org.virus.arravbot.api.script.framework.tasks.LoopTask;
import org.virus.arravbot.api.script.framework.tasks.OneTimeTask;
import org.virus.arravbot.api.script.framework.tasks.Task;
import org.virus.arravbot.api.script.utils.Time;
import org.virus.arravbot.api.script.utils.Validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 11:15 AM
 */
public abstract class Script {

    private final List<Task>      tasks;
    private final ExecutorService executor;

    protected Script() {
        tasks = Collections.synchronizedList(new ArrayList<Task>());
        executor = Executors.newFixedThreadPool(3);
    }

    public final void submit(Task task) {
        tasks.add(task);
    }

    public final void loop() {
        Task[] tasksCopy = tasks.toArray(new Task[tasks.size()]);

        for (final Task task : tasksCopy)
            if (task.valid()) {
                final Future<?> submit = executor.submit(new Runnable() {
                    @Override
                    public void run() {
                        if (task instanceof OneTimeTask) {
                            ((OneTimeTask) task).run();
                            tasks.remove(task);
                        } else
                            Time.sleep(((LoopTask) task).loop());
                    }
                });

                if (task instanceof OneTimeTask)
                    continue;

                Time.waitFor(new Validator() {
                    @Override
                    public boolean valid() {
                        return submit.isDone();
                    }
                }, 0);
            }
    }
}
