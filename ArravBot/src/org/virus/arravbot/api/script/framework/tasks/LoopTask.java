package org.virus.arravbot.api.script.framework.tasks;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 11:17 AM
 */
public interface LoopTask extends Task {

    /**
     * @return The pause length in milliseconds before invoking this method again.
     */
    public abstract int loop();
}
