package org.virus.arravbot.api.script.framework;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 9:50 AM
 */
public @interface ScriptManifest {
    String name();

    int[] version() default {1, 0};

    String discription() default "";

    String[] authors();
}
