package org.virus.arravbot.api.script.framework.tasks;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 11:22 AM
 */
public interface OneTimeTask extends Task {
    public void run();
}
