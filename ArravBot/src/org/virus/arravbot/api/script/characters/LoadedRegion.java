package org.virus.arravbot.api.script.characters;

import org.virus.arravbot.api.Filter;
import org.virus.arravbot.api.script.wrappers.Area;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 10:08 AM
 */
public class LoadedRegion {
    private final Area region;

    public LoadedRegion(Area region) {
        this.region = region;
    }

    public Area getRegion() {
        return region;
    }

    public boolean contains(Locatable locatable) {
        return locatable.getLocation().within(region);
    }

    public GameObject[] getLoadedWithin() {
        return getLoadedWithin(Filter.ACCEPT_ALL);
    }

    public GameObject[] getLoadedWithin(Filter<GameObject> objectFilter) {
        GameObject[] allLoaded = getAllLoaded();
        List<GameObject> objectList = new ArrayList<>();

        for (GameObject object : allLoaded)
            if (contains(object) && objectFilter.accept(object))
                objectList.add(object);

        return objectList.toArray(new GameObject[objectList.size()]);
    }

    public static GameObject[] getAllLoaded() {
        throw new UnsupportedOperationException();
        //TODO
    }
}
