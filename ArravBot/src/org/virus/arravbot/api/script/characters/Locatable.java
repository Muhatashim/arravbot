package org.virus.arravbot.api.script.characters;

import org.virus.arravbot.api.script.wrappers.Location;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 10:23 AM
 */
public interface Locatable {
    public Location getLocation();
}
