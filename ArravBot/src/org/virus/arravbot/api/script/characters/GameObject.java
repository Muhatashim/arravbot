package org.virus.arravbot.api.script.characters;

import org.virus.arravbot.api.script.wrappers.Location;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 10:08 AM
 */
public class GameObject implements Locatable {

    private Location location;

    @Override
    public Location getLocation() {
        return location;
    }
}
