package org.virus.arravbot.api.script.wrappers;

import org.virus.arravbot.api.script.characters.Locatable;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 10:36 AM
 */
public class Area extends Polygon {

    public Area(int[] xpoints, int[] ypoints, int npoints) {
        super(xpoints, ypoints, npoints);
    }

    public Area(Location... points) {
        if (points.length == 0)
            return;

        int plane = points[0].getPlane();
        for (Location location : points)
            if (location.getPlane() != plane)
                throw new UnsupportedOperationException("Multiple planes");
            else
                add(location);
    }

    public void add(Locatable locatable) {
        Location location = locatable.getLocation();

        addPoint(location.getX(), location.getY());
    }

    public boolean contains(Locatable locatable) {
        Location location = locatable.getLocation();

        return contains(location.getX(), location.getY());
    }
}
