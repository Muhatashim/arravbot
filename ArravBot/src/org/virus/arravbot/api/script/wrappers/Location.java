package org.virus.arravbot.api.script.wrappers;

import org.virus.arravbot.api.script.characters.Locatable;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 10:09 AM
 */
public class Location implements Locatable, Serializable {

    private final int x;
    private final int y;
    private final int plane;

    public Location(int x, int y, int plane) {
        this.x = x;
        this.y = y;
        this.plane = plane;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getPlane() {
        return plane;
    }

    public boolean within(Area area) {
        return area.contains(this);
    }

    public Location[] surrounding(boolean includeDiagonals) {
        Location[] surrounding = new Location[includeDiagonals ? 8 : 4];

        int i = 0;
        for (int x = -1; x <= 1; x++)
            for (int y = -1; y <= 1; y++)
                if (!includeDiagonals || x == 0 || y == 0)
                    surrounding[i++] = translate(x, y);

        return surrounding;
    }

    public Location translate(int xChange, int yChange) {
        return new Location(getX() + xChange, getY() + yChange, getPlane());
    }

    public Location translate(int xChange, int yChange, int planeChange) {
        return new Location(getX() + xChange, getY() + yChange, getPlane() + planeChange);
    }

    @Override
    public Location getLocation() {
        return this;
    }
}
