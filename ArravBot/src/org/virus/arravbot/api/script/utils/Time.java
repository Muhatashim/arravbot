package org.virus.arravbot.api.script.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 12:53 PM
 */
public class Time {
    public static boolean waitFor(Validator validator, int maxWaitTime) {
        Timer waitTimer = new Timer(maxWaitTime);

        while (!validator.valid() && (maxWaitTime == 0 || waitTimer.isRunning()))
            Time.sleep(100);

        return validator.valid();
    }

    public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String format(long ms) {
        long hr = TimeUnit.MILLISECONDS.toHours(ms);
        long min = TimeUnit.MILLISECONDS.toMinutes(ms - TimeUnit.HOURS.toMillis(hr));
        long sec = TimeUnit.MILLISECONDS.toSeconds(ms - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min));
        return String.format("%d:%02d:%02d", hr, min, sec);
    }
}
