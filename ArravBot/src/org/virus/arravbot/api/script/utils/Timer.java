package org.virus.arravbot.api.script.utils;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/4/13
 * Time: 11:48 AM
 */
public class Timer {

    private long endTime;
    private long startTime;

    public Timer(int msWait) {
        startTime = System.currentTimeMillis();
        endTime = startTime + msWait;
    }

    public void setWaitTIme(int msWait) {
        startTime = System.currentTimeMillis();
        endTime = startTime + msWait;
    }

    public long msTillFinished() {
        return endTime - System.currentTimeMillis();
    }

    public long msSinceStart() {
        return startTime - System.currentTimeMillis();
    }

    public boolean isRunning() {
        return System.currentTimeMillis() < endTime;
    }

    @Override
    public String toString() {
        return Time.format(msTillFinished());
    }
}
