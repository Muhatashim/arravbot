package org.virus.arravbot.api.script;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/31/13
 * Time: 9:41 PM
 */
public interface PaintListener extends Listener{
    public void onRepaint(Graphics g);
}
