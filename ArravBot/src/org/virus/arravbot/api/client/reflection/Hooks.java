package org.virus.arravbot.api.client.reflection;

import org.virus.simple.JSONArray;
import org.virus.simple.JSONObject;
import org.virus.simple.parser.JSONParser;
import org.virus.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/1/13
 * Time: 12:53 PM
 */
public class Hooks {

    private HashMap<String, Hook> map;

    public Hooks() {
        map = new HashMap<>();
    }

    public Hook get(Hook.HookType type) {
        return map.get(type.toString());
    }

    public Collection<Hook> values() {
        return map.values();
    }

    public Set<String> keySet() {
        return map.keySet();
    }

    public Set<Map.Entry<String, Hook>> entrySet() {
        return map.entrySet();
    }

    public void load(InputStream input) throws IOException {
        JSONParser parser = new JSONParser();

        try {
            Object parse = parser.parse(new InputStreamReader(input));
            JSONObject obj = (JSONObject) parse;

            JSONArray fieldsArray = (JSONArray) obj.get("fields");
            for (Object rawFieldEntry : fieldsArray) {
                JSONObject rawJSONFieldEntry = (JSONObject) rawFieldEntry;

                String name = (String) rawJSONFieldEntry.get("name");
                String fqfn = (String) rawJSONFieldEntry.get("fqfn");
                String fieldName = (String) rawJSONFieldEntry.get("fieldName");

                try {
                    map.put(name, new Hook(name, fqfn, fieldName));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
