package org.virus.arravbot.api.client.reflection;

import org.virus.arravbot.api.Filter;
import org.virus.arravbot.api.client.Client;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/1/13
 * Time: 1:43 PM
 */
public class Hook {

    private final String name;
    private final Member member;

    public Hook(String name, final String fqClassName, String memberName) {
        this.name = name;
        Class<?> theClass;

        Filter<Class> classFilter = new Filter<Class>() {
            @Override
            public boolean accept(Class aClass) {
                return aClass.getCanonicalName().equals(fqClassName);
            }
        };
        theClass=classFilter.getAccepted(Client.get().getLoader().getClasses());

        Member tempMember = null;
        try {
            tempMember = memberName.endsWith("()") ?
                    theClass.getDeclaredMethod(memberName) :
                    theClass.getDeclaredField(memberName);
        } catch (NoSuchMethodException | NoSuchFieldException | NullPointerException e) {
            System.out.println("Failed to bind " + name);
            e.printStackTrace();
        }
        member = tempMember;
    }

    public Hook(String name, Member member) {
        this.name = name;
        this.member = member;
    }

    public String getName() {
        return name;
    }

    public Object getValue(Object instance) throws IllegalAccessException, InvocationTargetException {
        if (member == null)
            return null;

        if (member instanceof Field) {
            Field fieldMember = (Field) member;

            fieldMember.setAccessible(true);
            return fieldMember.get(instance);
        }
        if (member instanceof Method)
            return invoke(instance);

        throw new IllegalArgumentException("Not field or method");
    }

    public Object invoke(Object instance, Object... params) throws InvocationTargetException, IllegalAccessException {
        if (member == null)
            return null;

        if (member instanceof Method) {
            Method methodMember = (Method) member;

            methodMember.setAccessible(true);
            return methodMember.invoke(instance, params);
        } else
            throw new IllegalArgumentException("Member is not a method");
    }

    @Override
    public String toString() {
        return "Hook{" +
                "name='" + name + '\'' +
                ", member=" + member +
                '}';
    }

    public static enum HookType {
        CLIENT,

        CHAT_LOG,
        CHAT_NAMES,

        COLLISION_FLAGS,

        MYPLAYER_SKILLS,
        MYPLAYER_OPTIONS,
        MYPLAYER_NAME;

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }
}
