package org.virus.arravbot.api.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 6/1/13
 * Time: 8:06 AM
 */
public class IOHelper {

    public static byte[] getByteArray(InputStream is) throws IOException {
        byte[] buff = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        int amtRead;
        while ((amtRead = is.read(buff)) != -1)
            baos.write(buff, 0, amtRead);

        return baos.toByteArray();
    }
}
