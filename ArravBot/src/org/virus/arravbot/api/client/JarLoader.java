package org.virus.arravbot.api.client;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/31/13
 * Time: 9:59 PM
 */
public class JarLoader {

    private final JarFile               jarFile;
    private final ClassLoader           loader;
    private       Map<String, byte[]>   classes;
    private       List<Class> classesList;

    public JarLoader(URI jarURI) throws URISyntaxException, IOException {
        this.jarFile = new JarFile(new File(jarURI));
        loader = new ByteClassLoader();
    }

    public void save() {
        //TODO
    }

    public Map<String, byte[]> loadByteClasses() throws URISyntaxException {
        classes = new HashMap<>();
        Enumeration<JarEntry> entries = jarFile.entries();

        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();

            if (entry.getName().endsWith(".class")) {
                try {
                    byte[] byteArray = IOHelper.getByteArray(
                            jarFile.getInputStream(entry));

                    classes.put(entry.getName().replace(".class", ""), byteArray);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return classes;
    }

    public List<Class> getClasses() {
        if (classesList != null)
            return classesList;

        classesList = new ArrayList<>();

        try {
            Set<String> names = this.classes != null ? this.classes.keySet() : loadByteClasses().keySet();

            for (String name : names)
                try {
                    classesList.add(loader.loadClass(name));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return classesList;
    }

    private class ByteClassLoader extends ClassLoader {

        @Override
        protected Class<?> findClass(final String name) throws ClassNotFoundException {
            byte[] classBytes = classes.get(name);
            if (classBytes != null) {
                return defineClass(name, classBytes, 0, classBytes.length);
            }

            throw new ClassNotFoundException(name);
        }
    }
}
