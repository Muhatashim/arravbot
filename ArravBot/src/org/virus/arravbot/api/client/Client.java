package org.virus.arravbot.api.client;

import org.virus.arravbot.api.Filter;
import org.virus.arravbot.api.client.reflection.Hooks;
import org.virus.arravbot.api.script.chat.ChatLog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/31/13
 * Time: 10:16 PM
 */
public class Client {

    private       JFrame  mainFrame;
    private       JPanel  painter;
    private final JButton test;

    private final  JarLoader           loader;
    private final  Hooks               hooker;
    private final  List<EventListener> listeners;
    private static Client              instance;

    public Client(File pserverJarFile) throws IOException, URISyntaxException {
        loader = new JarLoader(pserverJarFile.toURI());
        listeners = new ArrayList<>();
        hooker = new Hooks();
        test = new JButton("Test");

        test.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ChatLog.get().run();
            }
        });

        instance = this;
    }

    public void loadHooks() throws IOException {
        File file = new File("C:\\Users\\VOLT\\IdeaProjects\\ArravBot\\ArravBot\\src\\org\\virus\\arravbot\\api\\client\\reflection\\hooks.json");
        FileInputStream fis = new FileInputStream(file);
        hooker.load(fis);
    }

    public static Client get() {
        return instance;
    }

    public Hooks getHooker() {
        return hooker;
    }

    public JarLoader getLoader() {
        return loader;
    }

    public void init() throws IllegalAccessException, InstantiationException {
        List<Class> classes = loader.getClasses();
        Class<?> appletClass;

        Filter<Class> appletClassFilter = new Filter<Class>() {
            @Override
            public boolean accept(Class aClass) {
                return aClass.getName().equals("KI");
            }
        };
        appletClass = appletClassFilter.getAccepted(classes);


        try {
            Constructor appletInitilizer = appletClass.getDeclaredConstructor(String[].class);
            Field guiField = appletClass.getDeclaredField("BFMJ");

            appletInitilizer.newInstance(new String[1]);
            mainFrame = (JFrame) guiField.get(null);
            painter = (JPanel) mainFrame.getContentPane().getComponent(2);

            JMenuBar menu = (JMenuBar) mainFrame.getContentPane().getComponent(1);
            menu.add(test);
            menu.updateUI();

            mainFrame.setTitle("ABot");
            mainFrame.setLocationRelativeTo(null);
        } catch (NoSuchMethodException | InvocationTargetException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public void addListener(EventListener listener) {
        listeners.add(listener);
    }
}
