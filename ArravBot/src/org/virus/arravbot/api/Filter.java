package org.virus.arravbot.api;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/31/13
 * Time: 10:22 PM
 */
public abstract class Filter<E> {

    public static final Filter ACCEPT_ALL = new Filter() {
        @Override
        public boolean accept(Object o) {
            return true;
        }
    };

    public abstract boolean accept(E element);

    /**
     * @return true indicating that it is time to exit the loop or false to continue looping.
     */
    public boolean run(E t) {
        return false;
    }

    public final <T extends Iterable<? extends E>> boolean loop(T arr) {
        for (E element : arr)
            if (accept(element) && run(element))
                return true;
        return false;
    }

    //Not varags cause of heap pollution
    public final boolean loop(E[] elements) {
        for (E element : elements)
            if (accept(element) && run(element))
                return true;
        return false;
    }

    public final <T extends Iterable<? extends E>> List<E> allAccepted(T arr) {
        List<E> accepted = new ArrayList<>();

        for (E element : arr)
            if (accept(element))
                accepted.add(element);
        return accepted;
    }

    public final <T extends Iterable<? extends E>> E getAccepted(T arr) {
        for (E element : arr)
            if (accept(element))
                return element;
        return null;
    }

    public final E getAccepted(E[] arr) {
        for (E element : arr)
            if (accept(element))
                return element;
        return null;
    }

    public final List<E> allAccepted(E[] arr) {
        List<E> accepted = new ArrayList<>();

        for (E element : arr)
            if (accept(element))
                accepted.add(element);
        return accepted;
    }
}
