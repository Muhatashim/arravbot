package org.virus.arravbot;

import org.virus.arravbot.api.client.Client;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created with IntelliJ IDEA.
 * User: VOLT
 * Date: 5/31/13
 * Time: 9:14 PM
 */
public class Main {

    public static void main(String[] args) throws IOException, URISyntaxException, InstantiationException, IllegalAccessException {
        Client client = new Client(new File("C:\\Users\\VOLT\\Desktop\\arravclient.jar"));

        client.init();
        client.loadHooks();
    }
}
